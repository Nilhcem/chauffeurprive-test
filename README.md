# Chauffeur Privé - Technical Test

## Instructions

See `instructions.pdf`

## How to switch map provider

This project uses Mapbox native SDK for Maps, Location search, and Location geocoding matching.  
If you want to remove Mapbox, you'll have to:

* Change `res/layout/home_map_fragment_map.xml` to your new map implementation. For more flexibility, consider using [AirMapView][airmapview], or similar, with a native Mapbox wrapper instead.
* Change `res/layout/home_activity_search.xml` to your new search location autocomplete implementation
* Update `GeocodingRepository` datasource to your new geocoding provider.

[airmapview]: https://github.com/airbnb/AirMapView
