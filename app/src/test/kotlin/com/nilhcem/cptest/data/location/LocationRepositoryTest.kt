package com.nilhcem.cptest.data.location

import com.google.android.gms.location.FusedLocationProviderClient
import com.google.common.truth.Truth.assertThat
import com.nilhcem.cptest.data.location.room.RoomDataSource
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit

class LocationRepositoryTest {

    @JvmField @Rule val mockitoRule = MockitoJUnit.rule()

    @Mock private lateinit var database: RoomDataSource
    @Mock private lateinit var locationProvider: FusedLocationProviderClient

    lateinit var locationRepo: LocationRepository

    @Before fun setup() {
        locationRepo = LocationRepository(database, locationProvider)
    }

    @Test fun should_delete_old_locations_before_inserting_a_new_one_to_keep_max_15_locations_in_db() {
        runBlocking {
            // Given
            val location = AppLocation(AppLocation.Type.USER_LOCATION, "address", 0.1, 0.1)

            // When
            locationRepo.persistLocationAsync(location).await()

            // Then
            verify(database, times(1)).deleteOldLocations()
            verify(database, times(1)).insertLocation(location)

            assertThat(true).isTrue()
        }
    }
}
