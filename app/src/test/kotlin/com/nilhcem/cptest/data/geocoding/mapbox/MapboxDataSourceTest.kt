package com.nilhcem.cptest.data.geocoding.mapbox

import android.content.Context
import com.google.common.truth.Truth.assertThat
import com.mapbox.services.api.geocoding.v5.models.CarmenContext
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.Mockito.`when` as When

class MapboxDataSourceTest {

    @JvmField @Rule val mockitoRule = MockitoJUnit.rule()

    @Mock private lateinit var context: Context
    @Mock private lateinit var carmenfeature: CarmenFeature

    lateinit var datasource: MapboxDataSource

    @Before fun setup() {
        datasource = MapboxDataSource(context)
    }

    @Test fun should_format_address_with_nb_road_postcode_city() {
        // Given
        carmenfeature.apply {
            When(address).thenReturn("12")
            When(text).thenReturn("Rue de Paris")

            val postcode = createContextMock("postcode.42", "75001")
            val city = createContextMock("place.randomId", "Paris")
            When(context).thenReturn(listOf(postcode, city))
        }

        // When
        val formatted = callFormatAddressMethod(carmenfeature)

        // Then
        assertThat(formatted).isEqualTo("12 Rue de Paris 75001 Paris")
    }

    @Test fun should_skip_null_data_when_formatting_address() {
        carmenfeature.apply {
            When(text).thenReturn("Champs de Mars")

            val city = createContextMock("place.randomId", "Paris")
            When(context).thenReturn(listOf(city))
        }

        // When
        val formatted = callFormatAddressMethod(carmenfeature)

        // Then
        assertThat(formatted).isEqualTo("Champs de Mars Paris")
    }

    private fun createContextMock(key: String, value: String) = Mockito.mock(CarmenContext::class.java).apply {
        When(id).thenReturn(key)
        When(text).thenReturn(value)
    }

    private fun callFormatAddressMethod(carmenfeature: CarmenFeature) = with(datasource.javaClass.getDeclaredMethod("formatAddress", CarmenFeature::class.java, String::class.java)) {
        isAccessible = true
        invoke(datasource, carmenfeature, " ") as String
    }
}
