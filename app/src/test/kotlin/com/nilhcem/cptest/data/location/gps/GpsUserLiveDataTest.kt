package com.nilhcem.cptest.data.location.gps

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.common.truth.Truth.assertThat
import com.nilhcem.cptest.core.test.ext.getValueForTesting
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.data.location.AppLocation.Type.USER_LOCATION
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.Mockito.`when` as When

class LocationLiveDataTest {

    @JvmField @Rule val instantExecutorRule = InstantTaskExecutorRule()
    @JvmField @Rule val mockitoRule = MockitoJUnit.rule()

    @Mock private lateinit var locationProvider: FusedLocationProviderClient

    private lateinit var liveData: GpsUserLiveData

    @Before fun setup() {
        liveData = GpsUserLiveData(locationProvider)
    }

    @Test fun should_set_user_location_on_callback() {
        // Given
        val location = Mockito.mock(Location::class.java).apply {
            When(latitude).thenReturn(1.0)
            When(longitude).thenReturn(2.0)
        }

        // When
        val callback = getField<LocationCallback>("locationCallback")
        callback.onLocationResult(LocationResult.create(listOf(location)))

        // Then
        assertThat(liveData.getValueForTesting()).isEqualTo(AppLocation(USER_LOCATION, null, 1.0, 2.0))
    }

    @Test fun should_not_start_requesting_location_updates_when_location_permission_is_not_granted_yet() {
        // Given
        setField("isLocationPermissionGranted", false)
        setField("isRequestingLocationUpdates", false)

        // When
        liveData.getValueForTesting()

        // Then
        assertThat(getField<Boolean>("isRequestingLocationUpdates")).isFalse()
    }

    @Test fun should_start_requesting_location_updates_when_location_permission_is_granted() {
        // Given
        setField("isLocationPermissionGranted", true)
        setField("isRequestingLocationUpdates", false)

        // When
        liveData.getValueForTesting()

        // Then
        assertThat(getField<Boolean>("isRequestingLocationUpdates")).isTrue()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> getField(name: String): T {
        return liveData.javaClass.getDeclaredField(name).apply { isAccessible = true }.get(liveData) as T
    }

    private fun <T> setField(name: String, value: T) {
        liveData.javaClass.getDeclaredField(name).apply { isAccessible = true }.set(liveData, value)
    }
}
