package com.nilhcem.cptest.data.location.room

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.os.Build
import com.google.common.truth.Truth.assertThat
import com.nilhcem.cptest.BuildConfig
import com.nilhcem.cptest.core.android.database.AppDatabase
import com.nilhcem.cptest.core.test.ext.getValueForTesting
import com.nilhcem.cptest.data.location.AppLocation
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(Build.VERSION_CODES.LOLLIPOP), packageName = BuildConfig.APPLICATION_ID)
class RoomDataSourceTest {

    @JvmField @Rule val instantExecutorRule = InstantTaskExecutorRule()
    @JvmField @Rule val mockitoRule = MockitoJUnit.rule()

    lateinit var datasource: RoomDataSource
    var db: AppDatabase? = null
    lateinit var dao: LocationDao

    @Before fun setup() {
        db = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.application, AppDatabase::class.java).allowMainThreadQueries().build()
        dao = db!!.locationDao()
        datasource = RoomDataSource(dao)
    }

    @After fun closeDb() {
        db!!.close()
    }

    @Test fun should_delete_old_locations_to_keep_only_15() {
        // Given
        (0..20).forEach { i ->
            dao.insertLocation(Location().apply {
                address = "address"
                latitude = i.toDouble()
                longitude = i.toDouble()
            })
        }

        // When
        datasource.deleteOldLocations()

        // Then (we actually keep 14 because we will insert one just after)
        assertThat(dao.getAllLocations().getValueForTesting().size).isEqualTo(15 - 1)
    }

    @Test fun should_insert_location_to_db() {
        // Given
        val applocation = AppLocation(AppLocation.Type.USER_LOCATION, "address!!", 0.42, 0.88)

        // When
        datasource.insertLocation(applocation)

        // Then
        val dbLocations = dao.getAllLocations().getValueForTesting()
        assertThat(dbLocations.size).isEqualTo(1)
        with(dbLocations[0]) {
            assertThat(address).isEqualTo("address!!")
            assertThat(latitude).isEqualTo(0.42)
            assertThat(longitude).isEqualTo(0.88)
        }
    }
}
