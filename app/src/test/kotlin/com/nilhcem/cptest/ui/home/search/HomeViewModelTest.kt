package com.nilhcem.cptest.ui.home.search

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.os.Build
import com.nilhcem.cptest.BuildConfig
import com.nilhcem.cptest.data.geocoding.GeocodingRepository
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.data.location.LocationRepository
import com.nilhcem.cptest.ui.home.HomeViewModel
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.anyDouble
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.mockito.Mockito.`when` as When

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(Build.VERSION_CODES.LOLLIPOP), packageName = BuildConfig.APPLICATION_ID)
class HomeViewModelTest {

    @JvmField @Rule val instantExecutorRule = InstantTaskExecutorRule()
    @JvmField @Rule val mockitoRule = MockitoJUnit.rule()

    @Mock private lateinit var locationRepo: LocationRepository
    @Mock private lateinit var geocodingRepo: GeocodingRepository

    lateinit var viewmodel: HomeViewModel

    @Before fun setup() {
        viewmodel = HomeViewModel(geocodingRepo, locationRepo)
        When(geocodingRepo.loadAddressFromCoordinatesAsync(anyDouble(), anyDouble())).thenReturn(async(CommonPool) { Pair("result", false) })
        When(locationRepo.getSelectedLocation()).thenReturn(MutableLiveData<AppLocation>())
    }

    @Test fun should_fetch_address_when_location_is_null() {
        // Given
        val location = AppLocation(AppLocation.Type.SEARCHED_ADDRESS, null, 0.1, 0.1)

        // When
        viewmodel.setOnLocationSelected(location)

        // Then
        verify(geocodingRepo, times(1)).loadAddressFromCoordinatesAsync(anyDouble(), anyDouble())
    }

    @Test fun should_not_persist_location_with_errors() {
        // Given
        val location = AppLocation(AppLocation.Type.SEARCHED_ADDRESS, "unknown", 0.1, 0.1, "error")

        // When
        viewmodel.setOnLocationSelected(location)

        // Then
        verify(geocodingRepo, times(0)).loadAddressFromCoordinatesAsync(anyDouble(), anyDouble())
        verify(locationRepo, times(0)).persistLocationAsync(location)
    }
}
