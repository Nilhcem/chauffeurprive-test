package com.nilhcem.cptest.data.location.gps

import android.arch.lifecycle.LiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.nilhcem.cptest.core.android.log.Timber
import com.nilhcem.cptest.data.location.AppLocation

class GpsUserLiveData(private val client: FusedLocationProviderClient) : LiveData<AppLocation>() {

    private var isLocationPermissionGranted = false
    private var isRequestingLocationUpdates = false

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            val lastLocation = result.lastLocation
            value = AppLocation(AppLocation.Type.USER_LOCATION, null, lastLocation.latitude, lastLocation.longitude)
        }
    }

    override fun onActive() {
        Timber.d { "onActive" }
        if (isLocationPermissionGranted) {
            startRequestLocationUpdates()
        }
    }

    override fun onInactive() {
        Timber.d { "onInactive" }
        if (isLocationPermissionGranted) {
            stopRequestLocationUpdates()
        }
    }

    fun setLocationPermissionGranted() {
        if (!isLocationPermissionGranted && value == null) {
            startRequestLocationUpdates()
        }
        isLocationPermissionGranted = true
    }

    private fun startRequestLocationUpdates() {
        if (!isRequestingLocationUpdates) {
            isRequestingLocationUpdates = true
            client.requestLocationUpdates(LocationRequest.create(), locationCallback, null)
        }
    }

    private fun stopRequestLocationUpdates() {
        if (isRequestingLocationUpdates) {
            isRequestingLocationUpdates = false
            client.removeLocationUpdates(locationCallback)
        }
    }
}
