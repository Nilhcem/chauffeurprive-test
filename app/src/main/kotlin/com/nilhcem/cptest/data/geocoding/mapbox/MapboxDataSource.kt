package com.nilhcem.cptest.data.geocoding.mapbox

import android.content.Context
import android.support.annotation.WorkerThread
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding
import com.mapbox.services.api.geocoding.v5.models.CarmenContext
import com.mapbox.services.api.geocoding.v5.models.CarmenFeature
import com.mapbox.services.commons.models.Position
import com.nilhcem.cptest.BuildConfig
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.log.Timber
import java.io.IOException
import javax.inject.Inject

class MapboxDataSource @Inject constructor(private val context: Context) {

    @WorkerThread
    fun loadAddressFromCoordinates(latitude: Double, longitude: Double): Pair<String, Boolean> {
        val strUnknown = context.getString(R.string.home_search_address_unknown)
        val strSeparator = context.getString(R.string.home_search_address_separator)

        val client = MapboxGeocoding.Builder<MapboxGeocoding.Builder<*>>()
                .setAccessToken(BuildConfig.MAPBOX_TOKEN)
                .setCoordinates(Position.fromCoordinates(longitude, latitude))
                .setGeocodingType(GeocodingCriteria.TYPE_ADDRESS)
                .build()

        try {
            val response = client.executeCall()
            if (response.isSuccessful) {
                val results = response.body().features
                if (results.size > 0) {
                    val address = formatAddress(results[0], strSeparator)
                    if (address.isNotBlank()) {
                        return address to true
                    }
                }
            }
        } catch (e: IOException) {
            Timber.e(e) { "Error getting address from coordinates" }
            return context.getString(R.string.home_search_address_network_error) to false
        }

        return strUnknown to false
    }

    private fun formatAddress(feature: CarmenFeature, separator: String): String {
        val number = feature.address.nullIfBlank()
        val road = feature.text.nullIfBlank()
        val postCode = feature.context.getElement("postcode")
        val city = feature.context.getElement("place")

        return listOf(number, road, postCode, city).filter { it != null }.joinToString(separator = separator)
    }

    private fun String?.nullIfBlank() = if (this?.isBlank() ?: true) null else this
    private fun List<CarmenContext>.getElement(key: String) = filter { it.id.startsWith("$key.") }.map { it.text }.firstOrNull()?.nullIfBlank()
}
