package com.nilhcem.cptest.data.location.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface LocationDao {

    companion object {
        private const val MAX_ENTRIES_IN_DB = 15
    }

    @Query("SELECT * FROM location")
    fun getAllLocations(): LiveData<List<Location>>

    @Query("DELETE FROM location WHERE id NOT IN (SELECT id FROM location ORDER BY id DESC LIMIT ${MAX_ENTRIES_IN_DB - 1})")
    fun deleteOldLocations()

    @Insert
    fun insertLocation(location: Location)
}
