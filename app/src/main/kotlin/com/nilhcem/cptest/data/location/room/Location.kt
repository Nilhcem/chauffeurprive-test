package com.nilhcem.cptest.data.location.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Location {
    @PrimaryKey(autoGenerate = true) var id: Long = 0
    var address: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
}
