package com.nilhcem.cptest.data.location.room

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.nilhcem.cptest.data.location.AppLocation
import javax.inject.Inject

class RoomDataSource @Inject constructor(private val dao: LocationDao) {

    fun getAllLocations(): LiveData<List<AppLocation>> {
        return Transformations.map(dao.getAllLocations()) {
            it.map {
                AppLocation(AppLocation.Type.FROM_CACHE, it.address, it.latitude, it.longitude)
            }
        }
    }

    fun deleteOldLocations() {
        dao.deleteOldLocations()
    }

    fun insertLocation(appLocation: AppLocation) {
        dao.insertLocation(appLocation.toDbLocation())
    }

    fun AppLocation.toDbLocation(): Location {
        val newLocation = Location()
        newLocation.address = address!!
        newLocation.latitude = latitude
        newLocation.longitude = longitude
        return newLocation
    }
}
