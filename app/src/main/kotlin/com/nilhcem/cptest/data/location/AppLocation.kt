package com.nilhcem.cptest.data.location

data class AppLocation(val type: Type, val address: String?, val latitude: Double, val longitude: Double, val errorMessage: String? = null) {

    enum class Type {
        USER_LOCATION,
        SEARCHED_ADDRESS,
        MAP_SCROLLED,
        FROM_CACHE
    }
}
