package com.nilhcem.cptest.data.geocoding

import com.nilhcem.cptest.data.geocoding.mapbox.MapboxDataSource
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GeocodingRepository @Inject constructor(private val datasource: MapboxDataSource) {

    fun loadAddressFromCoordinatesAsync(latitude: Double, longitude: Double) = async(CommonPool) {
        datasource.loadAddressFromCoordinates(latitude, longitude)
    }
}
