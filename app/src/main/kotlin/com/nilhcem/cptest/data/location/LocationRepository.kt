package com.nilhcem.cptest.data.location

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.nilhcem.cptest.core.android.log.Timber
import com.nilhcem.cptest.data.location.gps.GpsUserLiveData
import com.nilhcem.cptest.data.location.room.RoomDataSource
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationRepository @Inject constructor(private val database: RoomDataSource, private val locationProvider: FusedLocationProviderClient) {

    fun getUserLocation(): GpsUserLiveData {
        return GpsUserLiveData(locationProvider)
    }

    fun getSelectedLocation(): MutableLiveData<AppLocation> {
        return MutableLiveData()
    }

    fun getAllCacheLocations(): LiveData<List<AppLocation>> {
        return database.getAllLocations()
    }

    fun persistLocationAsync(appLocation: AppLocation) = async(CommonPool) {
        require(appLocation.address!!.isNotBlank()) { "Can't save location. Address is null or blank" }

        Timber.d { "Delete old locations" }
        database.deleteOldLocations()

        Timber.d { "Insert new location in db" }
        database.insertLocation(appLocation)
    }
}
