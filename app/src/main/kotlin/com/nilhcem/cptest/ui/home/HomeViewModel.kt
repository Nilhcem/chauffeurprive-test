package com.nilhcem.cptest.ui.home

import android.arch.lifecycle.ViewModel
import com.nilhcem.cptest.core.android.log.Timber
import com.nilhcem.cptest.data.geocoding.GeocodingRepository
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.data.location.AppLocation.Type.FROM_CACHE
import com.nilhcem.cptest.data.location.AppLocation.Type.USER_LOCATION
import com.nilhcem.cptest.data.location.LocationRepository
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val geocodingRepo: GeocodingRepository, private val locationRepo: LocationRepository) : ViewModel() {

    val gpsLiveData by lazy { locationRepo.getUserLocation() }
    val locationLiveData by lazy { locationRepo.getSelectedLocation() }
    val cacheLiveData by lazy { locationRepo.getAllCacheLocations() }

    fun setOnLocationSelected(location: AppLocation) {
        Timber.i { "onLocationSelected: $location" }
        locationLiveData.value = location

        if (location.errorMessage == null) {
            if (location.address.isNullOrBlank()) {
                fetchAddressFromCoordinates(location)
            } else {
                persistLocation(location)
            }
        }
    }

    private fun fetchAddressFromCoordinates(location: AppLocation) {
        launch(UI) {
            with(geocodingRepo.loadAddressFromCoordinatesAsync(location.latitude, location.longitude).await()) {
                val value = first
                val success = second

                val newLocation = AppLocation(location.type, if (success) value else null, location.latitude, location.longitude, if (success) null else value)
                require(!newLocation.address.isNullOrBlank() || !newLocation.errorMessage.isNullOrBlank()) { "Address/error must be specified" }
                setOnLocationSelected(newLocation)
            }
        }
    }

    private fun persistLocation(location: AppLocation) {
        if (location.type != FROM_CACHE && location.type != USER_LOCATION) {
            launch(UI) {
                locationRepo.persistLocationAsync(location).await()
            }
        }
    }
}
