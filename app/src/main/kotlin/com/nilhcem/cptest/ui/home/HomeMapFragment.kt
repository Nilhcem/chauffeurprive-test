package com.nilhcem.cptest.ui.home

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.RequestCodes
import com.nilhcem.cptest.core.android.ext.bindView
import com.nilhcem.cptest.core.android.ext.startAppSettings
import com.nilhcem.cptest.core.android.log.Timber
import com.nilhcem.cptest.core.dagger.Injectable
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.ui.home.map.MapView
import com.nilhcem.cptest.ui.home.map.mapbox.MapboxMapView
import javax.inject.Inject

class HomeMapFragment : Fragment(), Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy { ViewModelProviders.of(activity, viewModelFactory).get(HomeViewModel::class.java) }

    private val mapView by lazy { view!!.findViewById<MapboxMapView >(R.id.map_view) as MapView }
    private val gpsFab by bindView<FloatingActionButton>(R.id.gps_fab)

    private val mapScrollEndLiveData by lazy { mapView.getScrollEndLiveData() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.home_map_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView.onCreate(savedInstanceState)

        gpsFab.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mapScrollEndLiveData.cancelPropagation()
                viewModel.gpsLiveData.value?.let { viewModel.setOnLocationSelected(it) }
            } else {
                requestLocationPermission()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()

        viewModel.gpsLiveData.observe({ lifecycle }) {
            it?.let { location ->
                mapView.setUserLocation(location)
                if (viewModel.locationLiveData.value == null) {
                    viewModel.setOnLocationSelected(location)
                }
            }
        }

        viewModel.locationLiveData.observe({ lifecycle }) {
            it?.let { location ->
                if (location.type != AppLocation.Type.MAP_SCROLLED) {
                    mapView.setSelectedLocation(location)
                }
            }
        }

        requestLocationPermission()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()

        mapScrollEndLiveData.observe(this, Observer {
            it?.let { location ->
                Timber.i { "Map scroll end. Location=$location" }
                viewModel.setOnLocationSelected(AppLocation(AppLocation.Type.MAP_SCROLLED, null, location.latitude, location.longitude))
            }
        })
    }

    override fun onPause() {
        super.onPause()
        mapScrollEndLiveData.removeObservers(this)
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == RequestCodes.PERMISSION_HOME_LOCATION && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewModel.gpsLiveData.setLocationPermissionGranted()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Snackbar.make(view!!, R.string.home_location_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.home_location_permission_rationale_button) {
                                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), RequestCodes.PERMISSION_HOME_LOCATION)
                            }
                            .show()
                } else {
                    Snackbar.make(view!!, R.string.home_location_permission_settings, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.home_location_permission_settings_button) {
                                activity.finish()
                                activity.startAppSettings()
                            }
                            .show()
                }
            }
        }
    }

    private fun requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            viewModel.gpsLiveData.setLocationPermissionGranted()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), RequestCodes.PERMISSION_HOME_LOCATION)
        }
    }
}
