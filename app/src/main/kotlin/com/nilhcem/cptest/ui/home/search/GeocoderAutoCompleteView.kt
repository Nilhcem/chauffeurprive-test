package com.nilhcem.cptest.ui.home.search

import com.nilhcem.cptest.data.location.AppLocation

interface GeocoderAutoCompleteView {

    fun init(onLocationSelectedListener: (AppLocation) -> Unit)

    fun setLocationText(address: String?)
}
