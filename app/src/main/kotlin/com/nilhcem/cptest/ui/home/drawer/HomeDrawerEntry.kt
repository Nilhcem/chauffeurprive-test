package com.nilhcem.cptest.ui.home.drawer

import android.view.ViewGroup
import android.widget.TextView
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.ext.bindView
import com.nilhcem.cptest.core.android.recyclerview.BaseViewHolder
import com.nilhcem.cptest.data.location.AppLocation

class HomeDrawerEntry(parent: ViewGroup) : BaseViewHolder(parent, R.layout.home_drawer_entry) {

    private val locationName by bindView<TextView>(R.id.drawer_entry_location)

    fun bindData(location: AppLocation, onClickListener: () -> Unit) {
        locationName.text = location.address

        itemView.setOnClickListener {
            onClickListener.invoke()
        }
    }
}
