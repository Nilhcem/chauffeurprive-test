package com.nilhcem.cptest.ui.home.map

import android.arch.lifecycle.LiveData
import android.os.Bundle
import com.nilhcem.cptest.data.location.AppLocation

data class LatLng(val latitude: Double, val longitude: Double)

interface MapView {

    fun onCreate(savedInstanceState: Bundle?)
    fun onStart()
    fun onResume()
    fun onPause()
    fun onStop()
    fun onLowMemory()
    fun onDestroy()
    fun onSaveInstanceState(outState: Bundle)
    fun getScrollEndLiveData(): MapViewScrollEndLiveData
    fun setUserLocation(location: AppLocation)
    fun setSelectedLocation(location: AppLocation)
}

abstract class MapViewScrollEndLiveData : LiveData<LatLng>() {

    abstract fun cancelPropagation()
}
