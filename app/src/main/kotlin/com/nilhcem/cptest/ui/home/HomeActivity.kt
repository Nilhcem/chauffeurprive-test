package com.nilhcem.cptest.ui.home

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.widget.ImageView
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.ext.bindView
import com.nilhcem.cptest.core.android.ext.hideOnScreenKeyboard
import com.nilhcem.cptest.ui.home.drawer.HomeDrawerAdapter
import com.nilhcem.cptest.ui.home.search.GeocoderAutoCompleteView
import com.nilhcem.cptest.ui.home.search.mapbox.MapboxGeocoderAutoCompleteView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class HomeActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java) }

    private val searchLocation by lazy { findViewById<MapboxGeocoderAutoCompleteView>(R.id.search_location) as GeocoderAutoCompleteView }
    private val drawerIcon by bindView<ImageView>(R.id.drawer_icon)
    private val drawerLayout by bindView<DrawerLayout>(R.id.drawer_layout)
    private val drawerList by bindView<RecyclerView>(R.id.location_history_list)

    private lateinit var drawerAdapter: HomeDrawerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
        initNavigationDrawer()
        searchLocation.init { viewModel.setOnLocationSelected(it) }

        observeLocationChanges()
        observeLocationCacheChanges()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun initNavigationDrawer() {
        drawerAdapter = HomeDrawerAdapter()
        drawerAdapter.listener = {
            viewModel.setOnLocationSelected(it)
            drawerLayout.closeDrawer(Gravity.START)
        }
        drawerList.layoutManager = LinearLayoutManager(this)
        drawerList.adapter = drawerAdapter

        drawerIcon.setOnClickListener {
            drawerLayout.openDrawer(Gravity.START)
        }
    }

    private fun observeLocationCacheChanges() {
        viewModel.cacheLiveData.observe({ lifecycle }) {
            it?.let { locations -> drawerAdapter.setLocations(locations) }
        }
    }

    private fun observeLocationChanges() {
        viewModel.locationLiveData.observe({ lifecycle }) {
            it?.let { location ->
                hideOnScreenKeyboard()
                searchLocation.setLocationText(location.address ?: location.errorMessage)
            }
        }
    }
}
