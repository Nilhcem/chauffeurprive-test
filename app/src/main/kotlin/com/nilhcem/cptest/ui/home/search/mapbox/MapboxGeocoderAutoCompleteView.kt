package com.nilhcem.cptest.ui.home.search.mapbox

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria
import com.nilhcem.cptest.BuildConfig
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.ext.bindView
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.ui.home.search.GeocoderAutoCompleteView
import com.mapbox.services.android.ui.geocoder.GeocoderAutoCompleteView as MapboxGeocoderAutocompleteView

class MapboxGeocoderAutoCompleteView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : FrameLayout(context, attrs, defStyle), GeocoderAutoCompleteView {

    init {
        LayoutInflater.from(context).inflate(R.layout.home_activity_search_mapbox, this, true)
    }

    private val mapboxSearch by bindView<MapboxGeocoderAutocompleteView>(R.id.mapbox_search)

    override fun init(onLocationSelectedListener: (AppLocation) -> Unit) {
        mapboxSearch.apply {
            setAccessToken(BuildConfig.MAPBOX_TOKEN)
            setType(GeocodingCriteria.TYPE_ADDRESS)
            setOnFeatureListener { feature ->
                val position = feature.asPosition()
                onLocationSelectedListener.invoke(AppLocation(AppLocation.Type.SEARCHED_ADDRESS, feature.placeName, position.latitude, position.longitude))
            }
        }
    }

    override fun setLocationText(address: String?) {
        if (address.isNullOrBlank()) {
            mapboxSearch.setHint(R.string.home_search_hint_loading)
        } else {
            mapboxSearch.setHint(R.string.home_search_hint_default)
        }

        mapboxSearch.clearFocus()
        mapboxSearch.setText(address)
    }
}
