package com.nilhcem.cptest.ui.home.map.mapbox

import android.graphics.PointF
import android.os.Handler
import android.view.MotionEvent
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.nilhcem.cptest.core.android.log.Timber
import com.nilhcem.cptest.ui.home.map.LatLng
import com.nilhcem.cptest.ui.home.map.MapViewScrollEndLiveData

class HomeMapScrollEndLiveData(private val mapView: MapView) : MapViewScrollEndLiveData(), MapView.OnMapChangedListener {

    companion object {
        private val CALLBACK_DELAY_MS = 300L
    }

    private var isUserMovingMap = false
    private var hasRegionChanged = false

    private var scrollEndHandler: Handler? = null
    private val scrollEndCallback = Runnable {
        if (!isUserMovingMap) {
            if (hasRegionChanged) {
                mapView.getMapAsync { value = getMapCenterCoordinates(it) }
            }
            mapView.removeOnMapChangedListener(this)
        }
    }

    override fun onActive() {
        Timber.d { "onActive" }
        scrollEndHandler = Handler()

        mapView.setOnTouchListener { _, event ->
            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> {
                    Timber.d { "touch event down" }
                    isUserMovingMap = true
                    hasRegionChanged = false
                    mapView.addOnMapChangedListener(this)
                }
                MotionEvent.ACTION_UP -> {
                    Timber.d { "touch event up" }
                    isUserMovingMap = false
                }
            }
            false
        }
    }

    override fun onInactive() {
        Timber.d { "onInactive" }
        mapView.removeOnMapChangedListener(this)
        scrollEndHandler!!.removeCallbacks(scrollEndCallback)
        scrollEndHandler = null
    }

    override fun onMapChanged(change: Int) {
        if (change == MapView.REGION_DID_CHANGE) {
            hasRegionChanged = true
        }

        scrollEndHandler?.apply {
            removeCallbacks(scrollEndCallback)
            postDelayed(scrollEndCallback, CALLBACK_DELAY_MS)
        }
    }

    override fun cancelPropagation() {
        mapView.removeOnMapChangedListener(this)
        scrollEndHandler!!.removeCallbacks(scrollEndCallback)
    }

    private fun getMapCenterCoordinates(map: MapboxMap): LatLng {
        val mapboxLatlng = map.projection.fromScreenLocation(PointF(0.5f * (mapView.right - mapView.left), 0.5f * (mapView.bottom - mapView.top)))
        return LatLng(mapboxLatlng.latitude, mapboxLatlng.longitude)
    }
}
