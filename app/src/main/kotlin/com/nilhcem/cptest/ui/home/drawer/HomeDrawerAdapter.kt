package com.nilhcem.cptest.ui.home.drawer

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.nilhcem.cptest.data.location.AppLocation

class HomeDrawerAdapter : RecyclerView.Adapter<HomeDrawerEntry>() {

    var listener: ((location: AppLocation) -> Unit)? = null

    private val locations = mutableListOf<AppLocation>()

    override fun onBindViewHolder(holder: HomeDrawerEntry, position: Int) {
        val location = locations[position]

        holder.bindData(location) {
            listener?.invoke(location)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeDrawerEntry {
        return HomeDrawerEntry(parent)
    }

    override fun getItemCount(): Int {
        return locations.size
    }

    fun setLocations(locations: List<AppLocation>) {
        this.locations.apply {
            clear()
            addAll(locations)
        }
        notifyDataSetChanged()
    }
}
