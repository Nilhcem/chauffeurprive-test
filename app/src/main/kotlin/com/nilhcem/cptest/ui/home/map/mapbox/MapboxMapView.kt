package com.nilhcem.cptest.ui.home.map.mapbox

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.nilhcem.cptest.BuildConfig
import com.nilhcem.cptest.R
import com.nilhcem.cptest.core.android.ext.bindView
import com.nilhcem.cptest.data.location.AppLocation
import com.nilhcem.cptest.ui.home.map.MapView
import com.nilhcem.cptest.ui.home.map.MapViewScrollEndLiveData
import com.mapbox.mapboxsdk.geometry.LatLng as MLatLng
import com.mapbox.mapboxsdk.maps.MapView as MMapView

class MapboxMapView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : FrameLayout(context, attrs, defStyle), MapView {

    private val mapView by bindView<MMapView>(R.id.mapbox_map_view)
    private var userMarker: Marker? = null

    init {
        Mapbox.getInstance(context, BuildConfig.MAPBOX_TOKEN)
        LayoutInflater.from(context).inflate(R.layout.home_map_fragment_map_mapbox, this, true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        mapView.onCreate(savedInstanceState)
    }

    override fun onStart() {
        mapView.onStart()
    }

    override fun onResume() {
        mapView.onResume()
    }

    override fun onPause() {
        mapView.onPause()
    }

    override fun onStop() {
        mapView.onStop()
    }

    override fun onLowMemory() {
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mapView.onSaveInstanceState(outState)
    }

    override fun getScrollEndLiveData(): MapViewScrollEndLiveData {
        return HomeMapScrollEndLiveData(mapView)
    }

    override fun setUserLocation(location: AppLocation) {
        val latLng = MLatLng(location.latitude, location.longitude)

        mapView.getMapAsync { map ->
            if (userMarker == null) {
                val icon = IconFactory.getInstance(context).fromResource(R.drawable.map_marker_gps_icon)
                userMarker = map.addMarker(MarkerOptions().icon(icon).position(latLng))
            } else {
                userMarker!!.position = latLng
            }
        }
    }

    override fun setSelectedLocation(location: AppLocation) {
        mapView.getMapAsync { map ->
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(MLatLng(location.latitude, location.longitude), 14.0))
        }
    }
}
