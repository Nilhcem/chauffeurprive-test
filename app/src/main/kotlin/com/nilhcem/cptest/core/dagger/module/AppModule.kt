package com.nilhcem.cptest.core.dagger.module

import android.arch.persistence.room.Room
import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.nilhcem.cptest.App
import com.nilhcem.cptest.core.android.database.AppDatabase
import com.nilhcem.cptest.data.location.room.LocationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = arrayOf(ViewModelModule::class))
class AppModule {

    @Singleton @Provides fun provideContext(app: App): Context {
        return app
    }

    @Singleton @Provides fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

    @Singleton @Provides fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "app.db").build()
    }

    @Singleton @Provides fun provideLocationDao(appDatabase: AppDatabase): LocationDao {
        return appDatabase.locationDao()
    }
}
