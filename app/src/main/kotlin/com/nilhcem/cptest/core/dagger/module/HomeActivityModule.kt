package com.nilhcem.cptest.core.dagger.module

import com.nilhcem.cptest.ui.home.HomeMapFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeMapFragment(): HomeMapFragment
}
