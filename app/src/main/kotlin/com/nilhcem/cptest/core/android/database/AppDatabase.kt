package com.nilhcem.cptest.core.android.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.nilhcem.cptest.data.location.room.Location
import com.nilhcem.cptest.data.location.room.LocationDao

@Database(entities = arrayOf(Location::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao
}
