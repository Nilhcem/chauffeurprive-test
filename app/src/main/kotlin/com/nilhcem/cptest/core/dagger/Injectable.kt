package com.nilhcem.cptest.core.dagger

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
