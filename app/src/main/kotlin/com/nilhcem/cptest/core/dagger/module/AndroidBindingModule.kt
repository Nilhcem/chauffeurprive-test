package com.nilhcem.cptest.core.dagger.module

import com.nilhcem.cptest.ui.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidBindingModule {

    @ContributesAndroidInjector(modules = arrayOf(HomeActivityModule::class))
    abstract fun contributeHomeActivity(): HomeActivity
}
