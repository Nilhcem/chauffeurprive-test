package com.nilhcem.cptest.core.dagger

import com.nilhcem.cptest.App
import com.nilhcem.cptest.core.dagger.module.AndroidBindingModule
import com.nilhcem.cptest.core.dagger.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        AndroidBindingModule::class)
)
interface AppComponent {

    @Component.Builder interface Builder {
        @BindsInstance fun application(application: App): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}
