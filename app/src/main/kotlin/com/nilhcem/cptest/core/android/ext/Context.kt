package com.nilhcem.cptest.core.android.ext

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.nilhcem.cptest.BuildConfig

fun Context.startAppSettings() {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)).apply {
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
    startActivity(intent)
}
