package com.nilhcem.cptest.core.debug.stetho

import android.content.Context
import com.facebook.stetho.Stetho

class StethoInitializer(private val context: Context) {

    fun init() {
        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context))
                .build())
    }
}
