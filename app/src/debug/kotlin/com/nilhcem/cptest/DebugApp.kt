package com.nilhcem.cptest

import android.os.StrictMode
import com.nilhcem.cptest.core.debug.stetho.StethoInitializer

class DebugApp : App() {

    val stetho by lazy { StethoInitializer(this) }

    override fun onCreate() {
        super.onCreate()
        enableStrictMode()
        stetho.init()
    }

    private fun enableStrictMode() {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .build())

        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build())
    }
}
